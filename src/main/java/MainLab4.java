public class MainLab4 {
    public static void main(String[] args) {
        System.out.println("Матрица");
        ProcessMatrix.ConclusionMatrix(Matrix.dan);
        System.out.println("Стандартизованная");
        ProcessMatrix.ConclusionMatrix(ProcessMatrix.StandartMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan),
                ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan))));
        System.out.println("Корреляционная");
        ProcessMatrix.ConclusionMatrix(lab4.corel);
        System.out.println("Корреляционная матрица значимо отличается от единичной");
        lab4.Otlichie();
        System.out.println("Собственные числа");
        ProcessMatrix.ConclusionMatrix(Matrix.sobstvCh);
        System.out.println("Сумма собственных чисел: "+ProcessMatrix.Okruglenie(lab4.sumSobCh()));
        System.out.println("I= "+lab4.I());
        System.out.println("Собственные векторы");
        ProcessMatrix.ConclusionMatrix(lab4.Y());
        System.out.println("Коэффициенты для 1 компоненты");
        ProcessMatrix.ConclusionMatrix(Matrix.GlComp1);
        System.out.println("Первая главная компонента");
        System.out.println(lab4.Conclusion(Matrix.GlComp1));
        System.out.println("-"+lab4.Naib(Matrix.GlComp1));
        System.out.println("Коэффициенты для 2 компоненты");
        ProcessMatrix.ConclusionMatrix(Matrix.GlComp2);
        System.out.println("Вторая главная компонента");

        System.out.println(lab4.Conclusion(Matrix.GlComp2));
        System.out.println("-"+lab4.Naib(Matrix.GlComp2));
    }

}
