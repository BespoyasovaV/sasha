public class MainClassLab2 {
    public static void main(String[] args) {
        double[][]Z=lab2.matrix();
        double[][]Y=lab2.stolb();
        System.out.println("X транспонированная");
        ProcessMatrix.ConclusionMatrix(lab2.transport(Z));
        double[][] transpon = lab2.transport(Z);
        System.out.println("X умноженное на Хт");
        ProcessMatrix.ConclusionMatrix(lab2.multiply(lab2.transport(Z),Z));
        System.out.println("обратная матрица от Х*Хт");
        ProcessMatrix.ConclusionMatrix(lab2.inversion(lab2.multiply(lab2.transport(Z),Z), 5));
        double[][] obr = lab2.inversion(lab2.multiply(lab2.transport(Z),Z), 5);
        double[][] obrNaranspon = lab2.multiply(obr, transpon);
        double[][] itog = lab2.multiply(obrNaranspon, Y);
        System.out.println("итоговый ответ");
        ProcessMatrix.ConclusionMatrix(itog);
        System.out.println("сравнение значений");
        System.out.println(ProcessMatrix.srzn(Y));
        System.out.println(lab2.srZn(itog));
        System.out.println("коэффициент детерминации");
        System.out.println(lab2.determ(Y,itog));
    }
}
