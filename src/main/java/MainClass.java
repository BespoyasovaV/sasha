public class MainClass {
    public static void main(String[] args) {

        System.out.println("Среднее значение по столбцам");
        ProcessMatrix.ConclusionArr(ProcessMatrix.ColumnMean(Matrix.dan));
        System.out.println();
        System.out.println("Дисперсия по столбцам");
        ProcessMatrix.ConclusionArr(ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan)));
        System.out.println();
        System.out.println("Стандартизованная матрица");
        ProcessMatrix.ConclusionMatrix(ProcessMatrix.StandartMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan),
                ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan))));
        System.out.println();
        System.out.println("Ковариационная матрица");
        ProcessMatrix.ConclusionMatrix(ProcessMatrix.CovaraMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan)));
        System.out.println();
        System.out.println("Кореляционая матрица");
        ProcessMatrix.ConclusionMatrix(ProcessMatrix.CorelMatrix(ProcessMatrix.StandartMatrix(Matrix.dan,
                ProcessMatrix.ColumnMean(Matrix.dan), ProcessMatrix.Dispersia(Matrix.dan,
                        ProcessMatrix.ColumnMean(Matrix.dan)))));
        System.out.println();
        System.out.println("Т расчетное");
        ProcessMatrix.ConclusionMatrix(ProcessMatrix.Trasch(ProcessMatrix.CorelMatrix(
                ProcessMatrix.StandartMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan),
                        ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan))))));
        System.out.println();
        System.out.println("Сравнение Т расчетного и Т табличного");
        ProcessMatrix.ConclusionMatrixString(ProcessMatrix.Comparison(ProcessMatrix.Trasch(ProcessMatrix.CorelMatrix(
                ProcessMatrix.StandartMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan),
                        ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan)))))));
        lab4.GlavnComp();
    }
}