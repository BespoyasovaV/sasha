public class lab4 {
    static double corel[][] = ProcessMatrix.CorelMatrix(ProcessMatrix.StandartMatrix(Matrix.dan,
            ProcessMatrix.ColumnMean(Matrix.dan), ProcessMatrix.Dispersia(Matrix.dan,
                    ProcessMatrix.ColumnMean(Matrix.dan))));
    static double stand[][] = ProcessMatrix.StandartMatrix(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan),
            ProcessMatrix.Dispersia(Matrix.dan, ProcessMatrix.ColumnMean(Matrix.dan)));

    public static double[][] Y() {
        double[][] y = new double[10][10];
        for (int i = 0; i < corel.length; i++) {
            for (int j = 0; j < corel[i].length; j++) {
                y[i][j] = corel[i][j] * Matrix.sobstvCh[i][0];
            }
        }

            for (int j = 0; j < 10; j++) {
                y[0][j] = Matrix.GlComp1[j][0];
                y[1][j] = Matrix.GlComp2[j][0];
            }

        return y;
    }


    public static void Otlichie() {
        double d = 0;
        for (int i = 0; i < corel.length; i++) {
            for (int j = 0; j < corel[0].length; j++) {
                if (i != j) {
                    d += Math.pow(corel[i][j], 2);
                }
            }
        }
        double X = corel.length * (corel.length - 1) / 2;
        System.out.println(corel.length * d + ">" + X);
    }

    public static double[] Jacobi(int N, double[][] A, double[] F, double[] X) {

        double[] TempX = new double[N];
        double norm;
        do {
            for (int i = 0; i < N; ++i) {
                TempX[i] = F[i];
                for (int g = 0; g < N; ++g) {
                    if (i != g)
                        TempX[i] -= A[i][g] * X[g];
                }
                TempX[i] /= A[i][i];
            }
            norm = Math.abs(X[0] - TempX[0]);
            for (int h = 0; h < N; h++) {
                if (Math.abs(X[h] - TempX[h]) > norm)
                    norm = Math.abs(X[h] - TempX[h]);
                X[h] = TempX[h];
            }
        } while (norm > 0.01);
        return TempX;
    }

    public static double sumSobCh() {
        double sum = 0.0;
        for (int i = 0; i < Matrix.sobstvCh.length; i++) {
            sum += Matrix.sobstvCh[i][0];
        }
        return sum;
    }

    public static double I() {
        double sum = 0.0;
        double sum1 = 0.0;
        for (int i = corel.length-1; i > 0; i--) {
            if (sum < 9.5) {
                sum += Matrix.sobstvCh[ corel.length-1-i][0];
            }
        }
        for (int j = 0; j < Matrix.sobstvCh.length; j++) {
            sum1 += Matrix.sobstvCh[j][0];
        }
        return sum / sum1;
    }

    public static String Conclusion(double[][] t) {
        String str = "y=";
        for (int i = 0; i < t.length; i++) {
            str += t[i][0] + "*x(" + i + ")+";
        }
        return str;

    }

    public static void GlavnComp() {
        double[][] y = new double[10][1];
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 1; k++) {
                y[i][k] += stand[i][k] * Matrix.sobstvCh[i][k];
            }
            ;
        }
    }

    public static double Naib(double[][] t) {
        double max = 0.0;
        for (int i = 0; i < t.length; i++) {
            if (max < Math.abs(t[i][0])) {
                max = Math.abs(t[i][0]);
            }
        }
        return max;
    }
}


