public class ProcessMatrix {
    public static double[] ColumnMean(double[][] matrix) {
        double[] sum = new double[10];
        double res = 0;
        for (int j = 0; j < 10; j++) {

            for (int i = 0; i < 75; i++) {
                res += matrix[i][j];
            }
            sum[j] = Okruglenie(res / 75);
            res = 0;
        }
        return sum;
    }

    public static double[] Dispersia(double[][] matrix, double[] srZnach) {
        double[] dis = new double[10];
        double resDis = 0;
        for (int j = 0; j < 10; j++) {

            for (int i = 0; i < 75; i++) {
                resDis += Math.pow((matrix[i][j] - srZnach[j]), 2);
            }
            dis[j] = Okruglenie(resDis / 75);
            resDis = 0;
        }
        return dis;
    }

    public static double[][] StandartMatrix(double[][] matrix, double[] srZnach, double[] dis) {
        double[][] standartMatrix = new double[75][10];
        for (int i = 0; i < matrix.length; i++) {

            for (int j = 0; j < matrix[i].length; j++) {
                standartMatrix[i][j] = Okruglenie((matrix[i][j] - srZnach[j]) / Math.sqrt(dis[j]));
            }
        }
        return standartMatrix;
    }

    public static double[][] CovaraMatrix(double[][] matrix, double[] srZnach) {
        double[][] covarMtrix = new double[10][10];


        for (int i = 0; i < covarMtrix.length; i++) {

            for (int j = 0; j < covarMtrix.length; j++) {
                double res = 0;
                for (double[] doubles : matrix) {
                    res += (doubles[i] - srZnach[i]) * (doubles[j] - srZnach[j]);
                }
                covarMtrix[i][j] = Okruglenie(res / 75);
            }

        }
        return covarMtrix;
    }

    public static double[][] CorelMatrix(double[][] standart) {
        double[][] corelMtrix = new double[10][10];
        for (int i = 0; i < corelMtrix.length; i++) {
            for (int j = 0; j < corelMtrix.length; j++) {
                double res = 0;
                for (int k = 0; k < Matrix.dan.length; k++) {
                    res += standart[k][i] * standart[k][j];
                }
                corelMtrix[i][j] = Okruglenie(res / 75.0);
            }

        }
        return corelMtrix;
    }

    public static double[][] Trasch(double[][] corelMatrix) {
        double[][] tRasch = new double[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                tRasch[i][j] = (corelMatrix[i][j] * Math.sqrt(75 - 2)) / Math.sqrt(1 - Math.pow(corelMatrix[i][j], 2));
            }
        }
        return tRasch;
    }

    public static String[][] Comparison(double[][] tRasch) {
        String[][] tComp = new String[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (Math.abs(tRasch[i][j]) >= Matrix.tTabl) {
                    tComp[i][j] = "H1";
                } else {
                    tComp[i][j] = "H0";
                }
            }
        }
        return tComp;
    }

    public static double Okruglenie(double value) {
        double scale = Math.pow(10, 2);
        return Math.ceil(value * scale) / scale;
    }

    public static void ConclusionArr(double[] arr) {
        for (double v : arr) {
            System.out.print(v + "    ");
        }
    }

    public static void ConclusionMatrix(double[][] matrix) {
        for (double[] doubles : matrix) {
            for (double aDouble : doubles) {
                System.out.printf("%25.7f\t", aDouble);
            }
            System.out.println();
        }
    }

    public static void ConclusionMatrixString(String[][] matrix) {
        for (String[] doubles : matrix) {
            for (String aDouble : doubles) {
                System.out.print("  " + aDouble);
            }
            System.out.println();
        }
    }
    public static double srzn(double[][] y){
        double sum = 0;
        for (int i = 0; i < y.length; i++) {
            for (int j = 0; j < y[0].length; j++) {
                sum += y[i][j];
            }
        }
        return 19.409;
    }
}
