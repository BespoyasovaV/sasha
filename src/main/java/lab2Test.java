public class lab2Test {
    public static double[][] transport(double[][] P) {
        double[][] C = new double[P[0].length][P.length];
        for (int i = 0; i < P.length; i++) {
            for (int j = 0; j < P[0].length; j++) {
                C[j][i] = P[i][j];
            }
        }
        return C;
    }

    public static double[][] multiply(double[][] X1, double[][] X2) {
        double[][] C = new double[X1.length][X2[0].length];
        for (int i = 0; i < X1.length; i++) {
            for (int j = 0; j < X2[0].length; j++) {
                for (int k = 0; k < X2.length; k++) {
                    C[i][j] += X1[i][k] * X2[k][j];
                }
            }
        }
        return C;
    }
    public static double[][] inversion(double [][]A, int N)
    {
        double temp;

        float [][] E = new float[10][10];


        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
            {
                E[i][j] = 0f;

                if (i == j)
                    E[i][j] = 1f;
            }

        for (int k = 0; k < N; k++)
        {
            temp = A[k][k];

            for (int j = 0; j < N; j++)
            {
                A[k][j] /= temp;
                E[k][j] /= temp;
            }

            for (int i = k + 1; i < N; i++)
            {
                temp = A[i][k];

                for (int j = 0; j < N; j++)
                {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int k = N - 1; k > 0; k--)
        {
            for (int i = k - 1; i >= 0; i--)
            {
                temp = A[i][k];

                for (int j = 0; j < N; j++) {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                A[i][j] = E[i][j];}}
        return  A;
    }

}
