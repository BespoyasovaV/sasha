
public class lab2 {
    public static double[][] transport(double[][] Z) {
        double[][] C = new double[Z[0].length][Z.length];
        for (int i = 0; i < Z.length; i++) {
            for (int j = 0; j < Z[i].length; j++) {
                C[j][i] = Z[i][j];
            }
        }
        return C;
    }

    public static double[][] multiply(double[][] X1, double[][] X2) {
        double[][] C = new double[X1.length][X2[0].length];
        for (int i = 0; i < X1.length; i++) {
            for (int j = 0; j < X2[0].length; j++) {
                for (int k = 0; k < X2.length; k++) {
                    C[i][j] += X1[i][k] * X2[k][j];
                }
            }
        }
        return C;
    }

    public static double[][] inversion(double[][] A, int N) {
        double temp;

        float[][] E = new float[10][10];


        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                E[i][j] = 0f;

                if (i == j)
                    E[i][j] = 1f;
            }

        for (int k = 0; k < N; k++) {
            temp = A[k][k];

            for (int j = 0; j < N; j++) {
                A[k][j] /= temp;
                E[k][j] /= temp;
            }

            for (int i = k + 1; i < N; i++) {
                temp = A[i][k];

                for (int j = 0; j < N; j++) {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int k = N - 1; k > 0; k--) {
            for (int i = k - 1; i >= 0; i--) {
                temp = A[i][k];

                for (int j = 0; j < N; j++) {
                    A[i][j] -= A[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                A[i][j] = E[i][j];
            }
        }
        return A;
    }

    public static double srZn(double[][] y) {
        double sum = 0;
        for (int i = 0; i < y.length; i++) {
            for (int j = 0; j < y[0].length; j++) {
                sum += y[i][j];
            }
        }
        return sum / y.length;
    }

    public static double[][] matrix() {
        double[][] Z = new double[75][5];
        for (int i = 0; i < Matrix.dan.length; i++) {
            for (int j = 0; j < 4; j++) {
                Z[i][j] = Matrix.dan[i][j];
            }
            Z[i][3] = Matrix.dan[i][7];
            Z[i][4] = 1.00;
        }
        return Z;
    }

    public static double[][] stolb() {
        double[][] Y = new double[75][1];
        for (int i = 0; i < Matrix.dan.length; i++) {
                Y[i][0] = Matrix.dan[i][8];
        }
        return Y;
    }
public static  double determ(double[][] Y, double[][]Ypol){
        double sumCh=0.0;
        double sumCh1=0.0;
        for(int i=0;i<Ypol.length;i++){
            sumCh+=(Y[i][0]+Ypol[i][0])*(Y[i][0]+Ypol[i][0]);
        }
    for(int i=0;i<Y.length;i++){
        sumCh1+=(Y[i][0]-19.409)*(Y[i][0]-19.409);
    }
    return 1-(sumCh/sumCh1);
}
}
