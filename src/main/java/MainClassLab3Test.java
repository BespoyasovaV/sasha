public class MainClassLab3Test {
    public static void main(String[] args) {

        System.out.println("X транспонированная");
        ProcessMatrix.ConclusionMatrix(lab2Test.transport(Matrix.p));
        double[][] transpon = lab2Test.transport(Matrix.p);
        System.out.println("X умноженное на Хт");
        ProcessMatrix.ConclusionMatrix(lab2Test.multiply(lab2Test.transport(Matrix.p),Matrix.p));
        System.out.println("обратная матрица от Х*Хт");
        ProcessMatrix.ConclusionMatrix(lab2Test.inversion(lab2Test.multiply(lab2Test.transport(Matrix.p),Matrix.p), 2));
        double[][] obr = lab2Test.inversion(lab2Test.multiply(lab2Test.transport(Matrix.p),Matrix.p), 2);
        double[][] obrNaranspon = lab2Test.multiply(obr, transpon);
        double[][] itog = lab2Test.multiply(obrNaranspon, Matrix.py);
        System.out.println("итоговый ответ");
        ProcessMatrix.ConclusionMatrix(itog);


    }
}
